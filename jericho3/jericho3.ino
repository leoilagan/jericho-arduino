
#define trigPin 13
#define echoPin 12
#define buzzer 11

String message;
void setup() {
  message = String();
 
  Serial.begin (115200);
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);
  pinMode(buzzer, OUTPUT);
 
}

void loop() {
  if(Serial.available()){
    Serial.print(Serial.read());
  }
  
  long duration, distance;
  digitalWrite(trigPin, LOW);  // Added this line
  delayMicroseconds(2); // Added this line
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10); // Added this line
  digitalWrite(trigPin, LOW);
  duration = pulseIn(echoPin, HIGH);
  distance = (duration/2) / 29.1;
  
// RANGE DETECTION & WARNING CODE 
  if (distance < 40) {  // Red alert: Danger close!
     message = "D#"; 
     message.concat(distance);
     Serial.println(message);
     beep(50); // Length of tone
    delay(1000); // Interval of beep. This short interval means that the object is very close.
   //Serial.write("DANGER"); // Send to Android for text to speech
   

}
  else if (distance >= 41 && distance < 55) {
    message = "W#";
    message.concat(distance);
    // Yellow alert: Obstacle Approaching
    Serial.println(message); // Send to Android for text to speech
    beep(100);
    delay(1000); // Interval of beep. This longer interval means that the object a bit far.
   
  }
 else if (distance >= 75 || distance == 0){ // No alert: No Obstacles
   message = "C#";
   message.concat(distance);
   Serial.println(message); // Send to Android for text to speech
   beep(50);
   delay(1000);  
  
  }

}
//-----------------Functions-------------------
// BEEP TONE BLOCK
void beep(unsigned char delayms){
  analogWrite(buzzer, 20);      // Almost any value can be used except 0 and 255
                           // experiment to get the best tone
  delay(delayms);          // wait for a delayms ms
  analogWrite(buzzer, 0);       // 0 turns it off
  delay(delayms);          // wait for a delayms ms   
}

